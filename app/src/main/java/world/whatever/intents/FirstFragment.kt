package world.whatever.intents

import android.content.Intent
import android.icu.util.Calendar
import android.net.Uri
import android.os.Bundle
import android.provider.CalendarContract
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.android.material.snackbar.Snackbar
import world.whatever.intents.databinding.FragmentFirstBinding

class FirstFragment : Fragment() {

	private var _binding: FragmentFirstBinding? = null

	// This property is only valid between onCreateView and
	// onDestroyView.
	private val binding get() = _binding!!

	override fun onCreateView(
		inflater: LayoutInflater, container: ViewGroup?,
		savedInstanceState: Bundle?
	): View? {

		_binding = FragmentFirstBinding.inflate(inflater, container, false)
		return binding.root

	}

	override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
		super.onViewCreated(view, savedInstanceState)

		binding.buttonPhone.setOnClickListener{
			Snackbar.make(view, "Don't call the rectorate", Snackbar.LENGTH_LONG)
				.setAction("Action", null).show()

			val dial_intent = Intent(Intent.ACTION_DIAL)
			dial_intent.data = Uri.parse("tel:" + "+420 576 035 065")
			startActivity(dial_intent)
		}

		binding.buttonMap.setOnClickListener{
			// tried also "Brno, Czech Republic", this one's more reliable
			val geo = Uri.parse("geo:0,0?q=Brno")
			val intent = Intent(Intent.ACTION_VIEW, geo)
			startActivity(intent)
		}

		binding.buttonWeb.setOnClickListener{
			val url = "https://www.utb.cz"
			val intent = Intent(Intent.ACTION_VIEW)
			intent.data = Uri.parse(url)
			startActivity(intent)
		}

		binding.buttonCal.setOnClickListener{
			val title = "Silvester"
			val date_begins: Calendar = Calendar.getInstance().apply {
				set(2021, 11, 23, 19, 0,0)
			}
			val date_ends: Calendar = Calendar.getInstance().apply {
				set(2021, 11, 23, 19, 1,0)
			}

			val intent = Intent(Intent.ACTION_INSERT).apply {
				type = "vnd.android.cursor.item/event"
				data = CalendarContract.Events.CONTENT_URI
				putExtra(CalendarContract.Events.TITLE, title)
				putExtra(CalendarContract.Events.EVENT_LOCATION, "North Pole")
				putExtra(CalendarContract.EXTRA_EVENT_BEGIN_TIME, date_begins.timeInMillis)
				putExtra(CalendarContract.EXTRA_EVENT_END_TIME, date_ends.timeInMillis)
				putExtra(CalendarContract.EXTRA_EVENT_ALL_DAY, false)
				putExtra(CalendarContract.Events.DESCRIPTION, "Best one ever!")
				putExtra(Intent.EXTRA_EMAIL, "it@fai.utb.cz,webmaster@fai.utb.cz")
				putExtra(CalendarContract.Events.AVAILABILITY, CalendarContract.Events.AVAILABILITY_FREE)
			}
			startActivity(intent)
		}
	}

	override fun onDestroyView() {
		super.onDestroyView()
		_binding = null
	}
}